from datetime import date


class Car:
    def __init__(self, brand: str, color: str, year: int) -> None:
        self.__validate_argument_types(brand=brand, color=color, year=year)
        self.__brand = brand
        self.__year = year
        self.__mileage = 0
        self.color = color

    def drive(self, distance) -> None:
        self.__validate_adding_milage(distance)
        self.__mileage += distance

    def get_age(self) -> int:
        return date.today().year - self.__year

    def repaint(self, color) -> None:
        self.color = color

    def get_milage(self) -> int:
        return self.__mileage

    def __str__(self) -> str:
        return f"Brand: {self.__brand}\nYear: {self.__year}\nMileage: {self.__mileage}\nColor: {self.color}"

    @staticmethod
    def __validate_adding_milage(distance: int) -> None:
        if distance < 0:
            raise ValueError("Distance can't be negative")

    @staticmethod
    def __validate_argument_types(**args) -> None:
        for arg in args.items():
            if arg[0] in ["brand", "color"] and isinstance(arg[1], str):
                raise TypeError(f"Argument {arg[0]} must be a string")
            elif arg[0] == "year" and isinstance(arg[1], int):
                raise ValueError(f"Argument {arg[0]} must be an integer")


if __name__ == '__main__':
    car_1 = Car("Mercedes", "White", 2023)
    car_2 = Car("Volvo", "Yellow", 2024)
    car_3 = Car("Toyota", "White", 2022)

    print(car_3)

