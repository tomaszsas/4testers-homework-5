from datetime import date

from assertpy import assert_that

from src.car import Car


def test_car_has_correct_color():
    car = Car("Mercedes", "White", 2023)
    expected_color = "white"
    assert assert_that(car.color.lower()).is_equal_to(expected_color)


def test_if_car_can_change_color():
    car = Car("Mercedes", "White", 2023)
    expected_color = "red"
    car.repaint(expected_color)
    assert assert_that(car.color).is_equal_to(expected_color)


def test_if_car_increase_milage():
    car = Car("Mercedes", "White", 2023)
    expected_mileage = 200
    car.drive(expected_mileage)
    assert assert_that(car.get_milage()).is_equal_to(expected_mileage)


def test_if_car_can_not_decrease_milage():
    car = Car("Mercedes", "White", 2023)
    assert assert_that(car.drive).raises(ValueError).when_called_with(-200)


def test_car_has_correct_age():
    car = Car("Mercedes", "White", 2023)
    expected_age = date.today().year - 2023
    assert_that(car.get_age()).is_equal_to(expected_age)


def test_car_has_correct_string_representation():
    car = Car("Mercedes", "White", 2023)
    expected_string = "Brand: Mercedes\nYear: 2023\nMileage: 0\nColor: White"
    assert assert_that(str(car)).is_equal_to(expected_string)


def test_instantiating_car_with_brand_as_int_type_raise_type_error():
    assert assert_that(Car).raises(TypeError).when_called_with(333, "White", 2023)


def test_instantiating_car_with_color_as_int_type_raise_type_error():
    assert assert_that(Car).raises(TypeError).when_called_with("Mercedes", 0, 2023)


def test_instantiating_car_with_year_as_string_raise_type_error():
    assert assert_that(Car).raises(TypeError).when_called_with("Mercedes", "White", "September")
